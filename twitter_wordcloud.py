import io
import base64
import wordcloud
import matplotlib.pyplot as plt
import nltk


def mk_cloud():
    f=open('/tmp/tweets.txt', 'r')
    text=f.read()
    f.close()

    nltk.download('stopwords')
    stopWords = set(nltk.corpus.stopwords.words('english'))

    cloud = wordcloud.WordCloud(stopwords=stopWords, background_color="white").generate(text)

    plt.figure(figsize=[30,15])
    plt.imshow(cloud, interpolation='bilinear')
    plt.axis("off")

    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)

    figdata_png = base64.b64encode(buf.getvalue())

    buf.close()

    img_tag = figdata_png.decode('UTF-8')

    return img_tag
