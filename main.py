from flask import Flask
from flask import render_template
from flask import request

import twitter_scrape_scrub
import twitter_wordcloud


app = Flask(__name__)


@app.route('/')
def get_username():
    return render_template('get_username.html')


@app.route('/', methods=['POST'])
def post_username():
    username = request.form['username']
    twitter_scrape_scrub.get_all_tweets(username)
    img = twitter_wordcloud.mk_cloud()
    return render_template('display_image.html', img=img)


if __name__ == '__main__':
    app.run(debug=True)
