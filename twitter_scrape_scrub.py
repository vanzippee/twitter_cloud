from flask import render_template
import tweepy
import json
import nltk
import re


def scrub_text(string):
    nltk.download('words')
    words = set(nltk.corpus.words.words())

    string=re.sub(r'[^a-zA-Z]+', ' ', string).lower()
    string=" ".join(w for w in nltk.wordpunct_tokenize(string)
                if w.lower() in words or not w.isalpha())
    return string


def get_all_tweets(username):
    with open('twitter_credentials.json') as cred_data:
        info=json.load(cred_data)
        consumer_key=info['API_KEY']
        consumer_secret=info['API_SECRET']
        access_key=info['ACCESS_TOKEN']
        access_secret=info['ACCESS_SECRET']

    screen_name = username

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    api=tweepy.API(auth)

    all_the_tweets=[]

    new_tweets=api.user_timeline(screen_name=screen_name, count=200)

    all_the_tweets.extend(new_tweets)

    oldest_tweet=all_the_tweets[-1].id - 1

    while len(new_tweets) > 0 and len(all_the_tweets) < 400:
        new_tweets=api.user_timeline(screen_name=screen_name, count=200,
                                     max_id=oldest_tweet)
        all_the_tweets.extend(new_tweets)
        oldest_tweet=all_the_tweets[-1].id -1

        print('...%s tweets downloaded' %len(all_the_tweets))

    outtweets=[[tweet.text.encode('utf-8')] for tweet in all_the_tweets]
    outtweets=scrub_text(str(outtweets))

    with open('/tmp/tweets.txt', 'w') as f:
        f.write(outtweets)
        f.close()
